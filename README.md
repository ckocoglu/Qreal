# The Official Qreal JS Style Guide

We have may different developers working on our codebase, and each person has their own way of writing code. However, through time, having everybody write code in their way can make our codebase inconsistent which is not pleasant. This style guide serves as the style guide which everybody should adhere to so that the codebase is kept in an excellent status.

## Correctness
Strive to make your code compile without warnings

## Naming
Descriptive and consistent naming makes software easier to read and understand. Use the following naming conventions. 
- Prioritize clarity over brevity
- Use camelCase (not snake_case)
- Use UpperCamelCase for class names (if using), lowerCamelCase for everything else
- Include all needed words and omit needless words
- Use terms that don’t surprise experts or confuse anyone else
- Avoid using abbreviations
- Choose good parameter names that serve as documentation
- Use US English spelling
    **Preferred:** 
      ``` var color = “red”; ```

    **Not Preferred:** 
     ``` var colour = “red;```

## Spacing
- Indent using 2 spaces rather than tabs to conserve spacec and help line wrapping.
- Method braces and other braces (```if```/```else```/```for```/```switch```/```while```)
    **Preferred:**
    ```
        if (isHappy){ 
            // Do something
        } else {
            // Do something else
        }
    ```
     **Not Preferred:**
    ```
        if (isHappy)
        {
            // Do something
        }
        else {
            // Do something else
        }
    ```
- There should be no blank lines after an opening brace or before a closing brace.
- Long lines should be wrapped at around 70 characters. A hard limit is intentionally not specified.
- Avoid trailing whitespaces at the ends of lines.
- Add a single newline character at the end of each file.

## Comments
When they are needed, use comments to explain why a particular piece of code does something. Comments must be kept up-to-date or deleted.

Avoid block comments inline with code, as the code should be as self-documenting as possible. Exception: This does not apply to those comments used to generate documentation.

Avoid the use of C-style comments (``` /* ... */ ```). Prefer the use of double- or triple-slash.

## Method Declarations
Keep short function declarations on one line including the opening brace:

```
    function sayHello(){
        // say hello code starts here     
    }
```

For functions having lots of parameters, put each parameter on a new line and add an extra indent on subsequent lines:

```
    function sayHello(
        name,
        surname,
        age,
        birthday,
        address,
        identity,
        phone,
        fax,
        email
    ){
        // say hello code starts here
    }
```
